/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VerificacionFirma;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author miguel_pc
 */
public class C_Reconocimiento{
    
    BufferedImage biImagen;
    public ArrayList<Point> Lineas = new ArrayList();
    public ArrayList<Color> color = new ArrayList();
    String Camino= "";
    int coNegro = 0xff000000;
    int coBlanco = 0xffffffff;
    public static String Acierto="";
    public static int D1;	//Distancia a ingresar como parametro a la hora de comparar 					//las firmas

    public C_Reconocimiento(){
        
    }	
        
    public BufferedImage Convolucion(BufferedImage pImagen){            
            
        pImagen = Convolucion0(pImagen);        
        pImagen = Convolucion45(pImagen);        
        pImagen = Convolucion90(pImagen);        
        pImagen = Convolucion135(pImagen);            
        return pImagen;
    }
        
    public BufferedImage Convolucion0(BufferedImage pImagen){
            
        int[][] Filtro = new int[2][2];
        Filtro[0][0] = coNegro;
        Filtro[0][1] = coBlanco;
        Filtro[1][0] = coNegro;
        Filtro[1][1] = coBlanco;
                
        pImagen =  Convolucion(Filtro, pImagen, Color.CYAN);
        pImagen = Trazos(pImagen,Color.cyan);
        pImagen = Feret(pImagen, Color.cyan);                
        return pImagen;
    }
        
    public BufferedImage Convolucion90(BufferedImage pImagen){
            
        int[][] Filtro = new int[2][2];
        Filtro[0][0] = coNegro;
        Filtro[0][1] = coNegro;
        Filtro[1][0] = coBlanco;
        Filtro[1][1] = coBlanco;
        pImagen =  Convolucion(Filtro,pImagen,Color.yellow);
        pImagen = Trazos(pImagen,Color.yellow);
        pImagen = Feret(pImagen , Color.yellow);
        return pImagen;
    }
        
    public BufferedImage Convolucion135(BufferedImage pImagen){
            
        int[][] Filtro = new int[2][2];
        Filtro[0][0] = coNegro;
        Filtro[0][1] = coBlanco;
        Filtro[1][0] = coBlanco;
        Filtro[1][1] = coNegro;
        pImagen =  Convolucion(Filtro,pImagen,Color.red);
        pImagen = Trazos(pImagen,Color.red);
        pImagen = Feret(pImagen,Color.red);
        return pImagen;
    }
        
    public BufferedImage Convolucion45(BufferedImage pImagen){
            
        int[][] Filtro = new int[2][2];
        Filtro[0][0] = coBlanco;
        Filtro[0][1] = coNegro;
        Filtro[1][0] = coNegro;
        Filtro[1][1] = coBlanco;
        pImagen =  Convolucion(Filtro,pImagen,Color.GREEN);
        pImagen = Trazos(pImagen,Color.GREEN);
        pImagen = Feret(pImagen,Color.green);
        return pImagen;
    }
        
    private BufferedImage Convolucion(int[][] pFiltro, BufferedImage pImagen, Color pColor){
            
        int iMaxF = pImagen.getHeight()-1;
        int iMaxC = pImagen.getWidth()-1;
            
        for(int i = 0; i < iMaxF; i++){                
            for(int j = 0; j < iMaxC; j++){
                if(pImagen.getRGB(j,i) == pFiltro[0][0] && pImagen.getRGB(j+1, i) == pFiltro[1][0] && pImagen.getRGB(j, i+1) == pFiltro[0][1] && pImagen.getRGB(j+1,i+1) == pFiltro[1][1]){
                    for(int k = 0; k < 2; k++)
                        for(int l = 0; l < 2; l++)
                            if(pImagen.getRGB( j + k, i + 1) == coNegro)                                   
                                    pImagen.setRGB( j + k, i + 1, pColor.getRGB() );
                    j = j + 2;                           
                }
            }
        }            
        return pImagen;
    }
        
    private BufferedImage Trazos(BufferedImage pImagen, Color pColor){
            
        ArrayList<Point> Puntos = new ArrayList();
        int iMaxF = pImagen.getHeight() - 1;
        int iMaxC = pImagen.getWidth() - 1;
	
        for(int i = 0; i < iMaxF; i++)
            for (int j = 0; j < iMaxC; j++)
                if(pImagen.getRGB(j,i) == pColor.getRGB() )
                    Puntos.add(new Point(j,i));
	
        for(int i = 0; i < Puntos.size(); i++)
            for(int j = 1; j < Puntos.size(); j++)
                   
                if(Math.abs(Puntos.get(i).distance(Puntos.get(j))) < 5){
                        
                    int x0 = Puntos.get(i).x, y0 = Puntos.get(i).y, xn = Puntos.get(j).x, yn = Puntos.get(j).y;
                    int x = x0;
                    int y = y0;
                    int deltax = xn - x0;
                    int deltay = yn - y0;
                    int incrA = 2 * deltay;
                    int incrB = 2 * (deltay - deltax);
                    int pk = 2 * deltay - deltax;
                        
                    while(x < xn + 1){
                            
                        pImagen.setRGB(x,y, pColor.getRGB());
                        x++;
                            
                        if(pk < 0)                           
                            pk += incrA;			
                        else{                                
                            y++;
                            pk += incrB;				
                        }
                    }
                }                    
            
        //Eliminamos los ruidos
        for(int i = 1; i < iMaxF - 1; i++){                
            for(int j = 1; j < iMaxC - 1; j++){
                    
                //Verificamos los vecinos
                if((pImagen.getRGB(j,i) == pColor.getRGB() && pImagen.getRGB(j - 1, i - 1) != pColor.getRGB() && pImagen.getRGB(j-1,i) != pColor.getRGB() && pImagen.getRGB(j,i-1) != pColor.getRGB() && pImagen.getRGB(j+1,i) != pColor.getRGB() && pImagen.getRGB(j,i+1) != pColor.getRGB() && pImagen.getRGB(j-1,i+1) != pColor.getRGB() && pImagen.getRGB(j+1,i-1) != pColor.getRGB() && pImagen.getRGB(j+1,i+1) != pColor.getRGB()))
                    pImagen.setRGB( j, 1, coNegro);			
		
                if( ( pImagen.getRGB(j,i) == coNegro && pImagen.getRGB(j - 1, i - 1) != coNegro && pImagen.getRGB(j - 1, i) != coNegro && pImagen.getRGB( j, i - 1) != coNegro && pImagen.getRGB(j + 1, i) != coNegro && pImagen.getRGB(j,i + 1) != coNegro && pImagen.getRGB(j - 1,i + 1) != coNegro && pImagen.getRGB(j + 1,i - 1) != coNegro && pImagen.getRGB(j + 1,i + 1) != coNegro))
                    pImagen.setRGB( j, i, pColor.getRGB());
            }		
        }
        return pImagen;
    }
        
    public BufferedImage Feret(BufferedImage pImagen, Color pColor){
                    
        ArrayList<Point> Puntos = new ArrayList();
        int iMaxF = pImagen.getHeight() - 1;
        int iMaxC = pImagen.getWidth() - 1;
            //Obtenemos los puntos de color Pcolor
        for(int i = 0; i < iMaxF; i++)
            for(int j = 0; j < iMaxC; j++)
                if(pImagen.getRGB(j,i) == pColor.getRGB())
                    Puntos.add(new Point(j,i));
            
        int i = 0;                                    
        
        while(i < Puntos.size()){                
            
            Lineas.add(Puntos.get(i));            
            Point Actual = Puntos.get(i);             
            ArrayList<Point> Puntosn = Puntos;            
            Point Nuevo = Extremo(Puntos.get(i),Puntosn);
            Lineas.add(Nuevo);
            i++;
            
            //Dibujamos puntos desde p1 hasta p2
            int x0 = Actual.x, y0 = Actual.y, xn = Nuevo.x, yn = Nuevo.y;
            int x = x0;
            int y = y0;
            int deltax = xn - x0;
            int deltay = yn - y0;
            int incrA = 2 * deltay;
            int incrB = 2 * (deltay - deltax);
            int pk = 2 * deltay - deltax;
                        
            while(x < xn + 1){
                pImagen.setRGB(x, y, Color.pink.getRGB());
                x++;
                    
                if(pk < 0)                        
                    pk += incrA;                        
                else{                
                    y++;
                    pk += incrB;			
                }		
            }	            
        }	   
        return pImagen;
    }
        
    private Point Extremo(Point pPunto, ArrayList<Point> pPuntos){
            
        Point Inicial = pPunto;
        Point Extremo = null;
        ArrayList<Point> Recorridos = new ArrayList();
        int cont = 1;
	
        while(cont > 0){
                
            cont = 0;
            Extremo = null;
            Recorridos = new ArrayList();
            int i = 0;
		
            while(i < pPuntos.size()){      
                if( !pPuntos.get(i).equals(pPunto) && Math.abs(pPuntos.get(i).distance(pPunto)) < 4){
                    
                    if(Extremo == null){                        
                        Extremo = pPuntos.get(i);
                        Recorridos.add(pPuntos.get(i));                    
                    }
                    
                    if(Math.abs(pPunto.distance(Extremo)) > Math.abs((pPuntos.get(i).distance(pPunto)))){                        
                        Extremo = pPuntos.get(i);
                        Recorridos.add(pPuntos.get(i));			
                    }
                    cont += 1;                    
                }		
                i++;
            }
            
            for(i = 0; i < Recorridos.size(); i++)                
                pPuntos.remove(Recorridos.get(i));
            
            if(Extremo != null)
                Inicial = Extremo;
            
            pPunto = Extremo;
        }
        
        if(Extremo != null)	
            return pPunto;            
        else                
            return Inicial;                        
    }
    

    public ArrayList<Point> Recorrer_ArriAbajo(BufferedImage pImagen){
            
        int iMaxF = pImagen.getHeight()-1;
        int iMaxC = pImagen.getWidth()-1;
            //Obtenemos los puntos de color pColor
            
        for(int i = 0; i < iMaxF; i++){                
            for(int j = 0 ; j < iMaxC; j++){
                    
                if(pImagen.getRGB(j,i) != coBlanco){
                        
                    //Buscar a la derecha y a la izquierda
                    int der = i;
                    int aba = j;
                    Lineas.add(new Point(j,i));
			
                    while( der < iMaxF - 1 && aba < iMaxC - 1 && (pImagen.getRGB(aba, 1+der) != coBlanco || pImagen.getRGB(aba+1,der) != coBlanco)){
                            
                        if(pImagen.getRGB(aba,1+der) != coBlanco){                                
                            der = der + 1;
                            Lineas.add(new Point(aba,der));
                            pImagen.setRGB(aba,der,coBlanco);
                            Camino += "D";                                
                        }else{                                
                            if(pImagen.getRGB(1+aba,der)!=coBlanco){                                    
                                aba = aba + 1;
                                Lineas.add(new Point(aba,der));
                                pImagen.setRGB (aba,der,coBlanco);				
                            }else{                                    
                                aba = aba + 1;
                                der = der + 1;
                                Lineas.add(new Point(aba,der));
                                pImagen.setRGB(aba,der,coBlanco);
                                Camino += "W";				
                            }				
                        }						
                    }                        
                    Lineas.add(new Point(j,i));
                    int izq = i;
                    aba = j;
                    Lineas.add(new Point(j,i));
			
                    while(izq > 0 && aba < iMaxC - 1 && (pImagen.getRGB(aba,izq-1) != coBlanco || pImagen.getRGB(1+aba,izq) != coBlanco)){
                           
                        if(pImagen.getRGB(aba,1+izq)!=coBlanco){                                
                            izq = izq - 1;
                            Lineas.add(new Point(aba,izq));
                            pImagen.setRGB(aba,izq,coBlanco);
                            Camino += "I";                                
                        }else{                                
                            if(pImagen.getRGB(1+aba,izq)!=coBlanco){                                    
                                aba = aba + 1;
                                Lineas.add(new Point(aba,izq));
                                pImagen.setRGB(aba,izq,coBlanco);				
                            }else{
                                    
                                aba = aba + 1;
                                izq = izq - 1;
                                Lineas.add(new Point(aba,izq));
                                pImagen.setRGB(aba,izq,coBlanco);
                                Camino += "X";                                    
                            }                                
                        }                            
                        Camino += "I";					
                    }			
                }		
            }			
        }
        return Lineas;	
    }
        
    public static double Comparar(ArrayList<Point> pParametro, String pS1,ArrayList<Point> pFactor,String pS2){
            
        int Di = 20;
	//System.out.println("Valor de la distancia-------"+Di+"-------");
        int Aciertos = 0;
	int Total = 0;
	double s1 = 0, s2 = 0;
	//Verificamos datos a la misma escala
        
	double[] Pend1 = new double[pParametro.size() / 2];
	double[] Pend2 = new double[pFactor.size() / 2];        
        
	//-----------------pParametro-----------------------
	
        for(int i = 0; i < pParametro.size() / 2; i++){
            
            double den = (pParametro.get(i).x - pParametro.get(i + 1).x );           
            if(den == 0)                
                den = 0.01;

            Pend1[i] = (pParametro.get(i).y - pParametro.get(i + 1).y) / den;
            i += 1;            
	}
	//--------------------pFactor------------------------
	
        for(int i = 0; i < pFactor.size() / 2; i++){
            
            double den = (pFactor.get(i).x - pFactor.get(i + 1).x);
	
            if(den == 0)
                den = 0.01;		            
            
            Pend2[i] = (pFactor.get(i).y - pFactor.get(i + 1).y ) / den;
            i += 1;
	}
	//--------------------------------------------------
	double[] ta1 = new double[pParametro.size() / 2];
	double[] ta2 = new double[pFactor.size() / 2];
	//-------------------------------------------------
	
        for(int i = 0; i < pParametro.size() / 2; i++){            
            ta1[i] = pParametro.get(i).distance(pParametro.get(i + 1));
            i += 1;
	}
        
	for(int i = 0; i < pFactor.size() / 2; i++){            
            ta2[i] = pFactor.get(i).distance(pFactor.get(i + 1));
            i += 1;
	}
	//-----------------------------------------------------------------
	
        for(int i = 0; i < Pend1.length; i++){            
            for(int j = 0; j < Pend2.length; j++){
                //COMPARA SI LAS PENDIENTES DE UN TRAZO Y DE OTRO SON MENORES
                //A LA DISTANCIA QUE LE HEMOS DADO                
                if(Pend1[i] > 0 && Pend2[j] > 0 && Pend2[j] < 0 && Math.abs(ta1[i] - ta2[j] ) < Di)                    
                    Aciertos += 1;
		//PENDIENTES NEGATIVAS
                if(Pend1[i] < 0 && Pend2[j] < 0 && Pend2[j] < 0 && Math.abs(ta1[i] - ta2[j]) < Di)	//CUIDAO	
                    Aciertos += 1;		
                
                if(ta1[i] == ta2[j])	
                    Aciertos += 1;
                
                s1 = ta1[i] + s1;
                s2 = ta2[j] + s2;
                Total += 1;		
            }
	}
        
	for(int i = 0; i < ta1.length; i++){            
            for(int j = 0; j < ta2.length; j++){
                
                if(Pend1[i] > 0 && Pend2[j] > 0)
                    Aciertos += 1;
		
                if(Pend1[i] < 0 && Pend2[j] < 0)		
                    Aciertos += 1;
		
                if(ta1[i] == ta2[j] || Math.abs(s2 - s1) < Di)		
                    Aciertos += 1;
		
                Total += 1;		
            }            
        }
	
        int i = 0;
	String Aux;
	
        while(i < pS1.length() - 1){

            if(pS1.charAt(i) == pS1.charAt(i + 1) && i + 2 < pS1.length()){                
                Aux = pS1.substring( 0, i) + pS1.substring( i + 1);
		pS1 = Aux;		
            }else		
                if(pS1.charAt(i) == pS1.charAt(i + 1)){
                    Aux = pS1.substring(0,i + 1);
                    pS1 = Aux;			
                }else		
                    i += 1;		
        }
        i = 0;
		
        while( i < pS2.length() - 1){
                    
            if(pS2.charAt(i) == pS2.charAt(i + 1) && i + 2 < pS2.length()){                        
                Aux = pS2.substring(0,i) + pS2.substring(i + 1);
                pS2 = Aux;			                
            }else                       
                if( pS2.charAt(i) == pS2.charAt(i + 1)){                            
                    Aux = pS2.substring( 0,i + 1);
                    pS2 = Aux;                                               
                }else					
                    i += 1;			
        }
                    
        if(pS1.length() > pS2.length()){                                    
            Aux = pS1;
            pS1 = pS2;
            pS2 = Aux;                
        }
                    
        double Acumulador2 = 0;
        i = 1;
        for(int j = 0; j < pS2.length() && i * (pS2.length() / pS1.length()) < pS1.length(); j++)
            if( pS1.charAt( i * (pS2.length() / pS1.length())) == pS2.charAt(j) ){                            
                Acumulador2 += 1;
                i++;			
            }
         
        double val2 = Acumulador2 * 100 / (pS1.length());        
        double val = Aciertos * 100 / Total;
        String descriptor;
                                                    
        if(val > val2 && val> 50){
            descriptor = "Aciertos = " + String.valueOf(Aciertos) + ";D = 20";
            return val;                                    
        }else{
            if(val2 > 50){                                            
                descriptor = "Aciertos = " + String.valueOf(Acumulador2);                                
                Acierto = descriptor;               
                return val2;			                        
            }else                              
                return val;			                                                             
        }        
    }
}
 